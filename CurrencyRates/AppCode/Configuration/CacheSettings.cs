﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyRates.AppCode.Configuration
{
    /// <summary>
    /// Represents <CacheSettings> configuration section
    /// </summary>
    public class CacheSettings
    {
        public int AbsoluteExpirationSpan { get; set; }
    }
}
