﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Options;

using CurrencyRates.Models;
using CurrencyRates.AppCode.Configuration;

namespace CurrencyRates.AppCode.Services
{
    /// <summary>
    /// Implementation of <see cref="HttpService"/> service provider for exchangeratesapi.io
    /// </summary>
    public class ExchangeratesapiHttpService : HttpService
    {
        protected CacheSettings __cacheSettings;

        public ExchangeratesapiHttpService(HttpClient client, IOptions<CacheSettings> options) : base(client)
        {
            client.BaseAddress = new Uri("https://api.exchangeratesapi.io");
            this.__cacheSettings = options.Value;
        }

        /// <summary>
        /// Retrieves rates for the specified currencies
        /// </summary>
        /// <param name="RatesRequest"><see cref="RatesRequest"/> instance</param>
        /// <returns><see cref="RatesResponse"/> instance</returns>
        public override async Task<RatesResponse> GetRates(RatesRequest RatesRequest)
        {
            var response = await this.Client.GetAsync($"/latest?base={RatesRequest.From}&symbols={RatesRequest.To}").ContinueWith<object>(base.getResponse);
            if (response.GetType() != typeof(string))
                return null;
            
            var result = JObject.Parse(response as string);
            var ratesResponse = new RatesResponse()
            {
                From = RatesRequest.From,
                Rates = (result["rates"] as JObject)?.Properties().Select(t => {
                    Decimal.TryParse(t.Value?.ToString(), out decimal rate);
                    return new RateInfo()
                    {
                        To = t.Name,
                        Rate = rate,
                        ExpireAt = DateTime.Now.AddMinutes(this.__cacheSettings.AbsoluteExpirationSpan)
                    };
                }).ToArray()
            };
        
            return ratesResponse;
        }
    }
}
