﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

using CurrencyRates.Models;
using CurrencyRates.AppCode.Configuration;

namespace CurrencyRates.AppCode.Services
{
    public interface IRateService
    {
        Task<RatesResponse> GetRates(RatesRequest RatesRequest);
    }

    /// <summary>
    /// Service providing obtaining data about currency rates from different sources
    /// </summary>
    public class RatesService : IRateService
    {
        protected HttpService __exchangeratesapiService;
        protected IDistributedCache __cache;
        protected CacheSettings __cacheSettings;

        public RatesService(ExchangeratesapiHttpService ExchangeratesapiService, IDistributedCache Cache, IOptions<CacheSettings> options)
        {
            this.__exchangeratesapiService = ExchangeratesapiService;
            this.__cache = Cache;
            this.__cacheSettings = options.Value;
        }

        /// <summary>
        /// Retrieves rates for the specified currencies via <see cref="HttpService"/> instances
        /// </summary>
        /// <param name="RatesRequest"><see cref="RatesRequest"/> instance</param>
        /// <returns><see cref="RatesResponse"/> instance</returns>
        /// <remarks>Uses caching</remarks>
        public async Task<RatesResponse> GetRates(RatesRequest RatesRequest)
        {
            var cacheName = $"{RatesRequest.From}-{RatesRequest.To}";
            var cachedValue = await this.__cache.GetStringAsync(cacheName);

            RatesResponse rates;

            if (!String.IsNullOrEmpty(cachedValue))
            {
                rates = JsonConvert.DeserializeObject<RatesResponse>(cachedValue);
            }
            else
            {
                rates = await this.__exchangeratesapiService.GetRates(RatesRequest);
                byte[] encodedValue = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(rates));
                var options = new DistributedCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(this.__cacheSettings.AbsoluteExpirationSpan));
                await this.__cache.SetAsync(cacheName, encodedValue, options);
            }

            return rates;
        }
    }
}
