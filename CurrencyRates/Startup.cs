﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

using CurrencyRates.AppCode.Services;
using CurrencyRates.AppCode.Configuration;
using CurrencyRates.Models;

namespace CurrencyRates
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            // Configuration
            var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath);
            builder.AddXmlFile("config.xml", optional: false, reloadOnChange: true);
            this.Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    // Ignore loop references while JSON serialization
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    // User "PascalCase" instead defalt "camelCase"
                    options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                });

            // Add distributed cache provided through SQL
            services.AddDistributedSqlServerCache(options =>
            {
                options.ConnectionString = this.Configuration.GetConnectionString("DistributedCache");
                options.SchemaName = "dbo";
                options.TableName = "Cache";
            });

            // HTTP-client factory
            services.AddHttpClient<ExchangeratesapiHttpService>();

            // Configuration
            services.AddOptions();
            services.AddTransient(provider => this.Configuration);
            // Services
            services.AddTransient<IRateService, RatesService>();
            // IOptions
            services.Configure<CacheSettings>(this.Configuration.GetSection("CacheSettings"));

            //Entity Framework Core
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection"))
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
