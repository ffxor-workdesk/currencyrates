﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace CurrencyRates.Models
{
    public class RatesRequest
    {
        public string From { get; set; }
        public string To { get; set; }
    }
}
