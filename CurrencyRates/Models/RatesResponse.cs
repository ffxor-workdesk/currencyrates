﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyRates.Models
{
    public class RatesResponse
    {
        public string From { get; set; }
        public RateInfo[] Rates { get; set; }
    }
}
