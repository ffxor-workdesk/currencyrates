# Currency rates sample

This sample demonstrates interaction with external currency rates API services (for example here implementation for [exchangeratesapi.io](http://exchangeratesapi.io/) is shown).

Also, the application uses distributed SQL cache and logs requests of API.

The entry point `/Rates/{From}/{To?}` (`To` is an optional parameter) displays rates of conversion from one currency to another. If `To` is omitted, the result is about rates for all supported currencies.

## Configuration

Configuration settings contains in `config.xml` file.

- `	DistributedCache` - SQL connection string for distributed SQL cache;
- `DefaultConnection` - default SQL connection sting for the DB;
- `AbsoluteExpirationSpan` - time span for caching values (in minutes);

## Deploy

1. Initialize DB with `db.sql` file or create the table for caching manually via command:

   ```
   dotnet sql-cache create "Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=localdb;Integrated Security=True;" dbo Cache
   ```

   Where in quotes - your `DistributedCache` connection string;

2. Create the table `RequestsLog` from `db.sql`;

## Testing

There the few unit tests present:

- If `/Rates` returns a list of rates if `To` parameter is omitted;
- If `/Rates` returns the one rate if `To` parameter is specified;
- If `/Rates` returns `Bad request` if there are no parameters were specified;

For mocking the application DB context of `EF Core` the `Microsoft.EntityFrameworkCore.InMemory` is used.

